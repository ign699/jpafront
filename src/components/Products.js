import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.css'

export default class Products extends Component {

    state = {
        products: []
    }
    componentDidMount() {
        axios.get('http://localhost:8080/products')
            .then(res => {
                this.setState({products: res.data})
            })
    }


    render() {
        return (
            <div >
                <ul>
                    {
                        this.state.products.map(product => {
                            return <li key={product[0]}>name: {product[1]} inStock: {product[2]} </li>
                        })
                    }
                </ul>
            </div>
        )
    }

}

