import React, { Component } from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.css'

export default class PlaceOrder extends Component {

    state = {
        products: [],
        customers: [],
        prodQ: {},
        cust: ''
    }
    componentDidMount() {
        axios.get('http://localhost:8080/products')
            .then(res => {
                this.setState({products: res.data})
            })
        axios.get('http://localhost:8080/customers')
            .then(res => {
                this.setState({customers: res.data, cust: res.data[0][0]})
            })
    }

    onClick = (e)=>{
        const bod = Object.keys(this.state.prodQ).map(key => {
            return [parseInt(key), parseInt(this.state.prodQ[key])]
        })
        bod.unshift([this.state.cust, this.state.cust])
        console.log(bod)
        axios.post('http://localhost:8080/order', bod).
            then(res => {
                console.log(res)
        })
    }

    render() {
        return (
            <div >
                <select onChange={(e) => this.setState({cust: e.target.value})}>
                    {
                        this.state.customers.map(customer => {
                            return <option key={customer[0]} value={customer[0]}>{customer[1]}</option>
                        })
                    }
                </select>
                <table>
                    <thead>
                        <tr>
                            <th>
                               Product
                            </th>
                            <th>
                                Quantity
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.products.map(product => {
                            return <tr value={product[0]}>
                                <td>
                                    {product[1]}
                                </td>
                                <td>
                                    <input type={"number"} max={product[2]} min={0} onChange={(event) => {
                                        const e = event.target.value;
                                        this.setState(
                                            prevState => {
                                                prevState.prodQ[product[0]] = e
                                                return {
                                                    prodQ: prevState.prodQ
                                                }
                                            }
                                    )}}/>
                                </td>
                            </tr>
                        })
                    }
                    </tbody>
                </table>
                <button onClick={this.onClick}>
                    place
                </button>
            </div>
        )
    }

}