import React, { Component } from 'react'
import axios from 'axios'
import { Link } from "react-router-dom";

export default class Menu extends Component {

    state = {
        products: []
    }
    componentDidMount() {
        axios.get('http://localhost:8080/products')
            .then(res => {
                this.setState({products: res.data})
            })
    }


    render() {
        return (
            <nav className="navbar navbar-default navbar-fixed-top">
                <div className="container">
                    <Link to={"/products"}>Products  </Link>
                    <Link to={"/customers"}>  Customers</Link>
                    <Link to={"/createOrder"}>  Create Order</Link>
                </div>
            </nav>
        )
    }

}