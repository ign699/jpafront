import React, { Component } from 'react'
import axios from 'axios'

export default class Customers extends Component {

    state = {
        customers: []
    }
    componentDidMount() {
        axios.get('http://localhost:8080/customers')
            .then(res => {
                this.setState({customers: res.data})
            })
    }


    render() {
        return (
            <div>
                <ul>
                    {
                        this.state.customers.map(product => {
                            return <li key={product[0]}>name: {product[1]}</li>
                        })
                    }
                </ul>
            </div>
        )
    }

}

