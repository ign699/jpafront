import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Products from './components/Products'
import Customers from './components/Customers'
import 'bootstrap/dist/css/bootstrap.css'

import {BrowserRouter, Link, Route, Switch} from 'react-router-dom'
import Menu from "./components/Menu";
import PlaceOrder from "./components/PlaceOrder";

class App extends Component {
  render() {
    return (
        <div style={{display: "flex"}}>

            <Menu/>
            <div style={{marginTop: 100}}>
                <Switch>
                    <Route path={'/products'} component={Products}/>
                    <Route path={'/customers'} component={Customers}/>
                    <Route path={'/createOrder'} component={PlaceOrder}/>
                </Switch>
            </div>
        </div>
    );
  }
}

export default App;
